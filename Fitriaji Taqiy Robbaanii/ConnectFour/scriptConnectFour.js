function reloadPage() {
    location.reload();
}

var columns0 = document.querySelectorAll("#col0 div"),
    columns1 = document.querySelectorAll("#col1 div"),
    columns2 = document.querySelectorAll("#col2 div"),
    columns3 = document.querySelectorAll("#col3 div"),
    columns4 = document.querySelectorAll("#col4 div"),
    columns5 = document.querySelectorAll("#col5 div"),
    columns6 = document.querySelectorAll("#col6 div"),
    blue_red = 0;


    for(var i = 0; i < columns0.length; i++){
         columns0[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns0[columns0.length-1].className !== "turnBlue")&&(columns0[columns0.length-1].className !== "turnRed")){
                        return columns0[columns0.length-1].className = "turnBlue";

                    }
                    if((columns0[columns0.length-2].className === "circle")&&((columns0[columns0.length-1].className === "turnBlue")||(columns0[columns0.length-1].className === "turnRed"))){
                        return columns0[columns0.length-2].className = "turnBlue";
                    }

                    if((columns0[columns0.length-3].className === "circle")&&((columns0[columns0.length-2].className === "turnBlue")||(columns0[columns0.length-2].className === "turnRed"))){
                        return columns0[columns0.length-3].className = "turnBlue";
                    }

                    if((columns0[columns0.length-4].className === "circle")&&((columns0[columns0.length-3].className === "turnBlue")||(columns0[columns0.length-3].className === "turnRed"))){
                        return columns0[columns0.length-4].className = "turnBlue";
                    }

                    if((columns0[columns0.length-5].className === "circle")&&((columns0[columns0.length-4].className === "turnBlue")||(columns0[columns0.length-4].className === "turnRed"))){
                        return columns0[columns0.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns0[columns0.length-1].className !== "turnBlue")&&(columns0[columns0.length-1].className !== "turnRed")){
                        return columns0[columns0.length-1].className = "turnRed";

                    }
                    if((columns0[columns0.length-2].className === "circle")&&((columns0[columns0.length-1].className === "turnBlue")||(columns0[columns0.length-1].className === "turnRed"))){
                        return columns0[columns0.length-2].className = "turnRed";
                    }

                    if((columns0[columns0.length-3].className === "circle")&&((columns0[columns0.length-2].className === "turnBlue")||(columns0[columns0.length-2].className === "turnRed"))){
                        return columns0[columns0.length-3].className = "turnRed";
                    }

                    if((columns0[columns0.length-4].className === "circle")&&((columns0[columns0.length-3].className === "turnBlue")||(columns0[columns0.length-3].className === "turnRed"))){
                        return columns0[columns0.length-4].className = "turnRed";
                    }

                    if((columns0[columns0.length-5].className === "circle")&&((columns0[columns0.length-4].className === "turnBlue")||(columns0[columns0.length-4].className === "turnRed"))){
                        return columns0[columns0.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns1.length; i++){
        columns1[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns1[columns1.length-1].className !== "turnBlue")&&(columns1[columns1.length-1].className !== "turnRed")){
                        return columns1[columns1.length-1].className = "turnBlue";

                    }
                    if((columns1[columns1.length-2].className === "circle")&&((columns1[columns1.length-1].className === "turnBlue")||(columns1[columns1.length-1].className === "turnRed"))){
                        return columns1[columns1.length-2].className = "turnBlue";
                    }

                    if((columns1[columns1.length-3].className === "circle")&&((columns1[columns1.length-2].className === "turnBlue")||(columns1[columns1.length-2].className === "turnRed"))){
                        return columns1[columns1.length-3].className = "turnBlue";
                    }

                    if((columns1[columns1.length-4].className === "circle")&&((columns1[columns1.length-3].className === "turnBlue")||(columns1[columns1.length-3].className === "turnRed"))){
                        return columns1[columns1.length-4].className = "turnBlue";
                    }

                    if((columns1[columns1.length-5].className === "circle")&&((columns1[columns1.length-4].className === "turnBlue")||(columns1[columns1.length-4].className === "turnRed"))){
                        return columns1[columns1.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns1[columns1.length-1].className !== "turnBlue")&&(columns1[columns1.length-1].className !== "turnRed")){
                        return columns1[columns1.length-1].className = "turnRed";

                    }
                    if((columns1[columns1.length-2].className === "circle")&&((columns1[columns1.length-1].className === "turnBlue")||(columns1[columns1.length-1].className === "turnRed"))){
                        return columns1[columns1.length-2].className = "turnRed";
                    }

                    if((columns1[columns1.length-3].className === "circle")&&((columns1[columns1.length-2].className === "turnBlue")||(columns1[columns1.length-2].className === "turnRed"))){
                        return columns1[columns1.length-3].className = "turnRed";
                    }

                    if((columns1[columns1.length-4].className === "circle")&&((columns1[columns1.length-3].className === "turnBlue")||(columns1[columns1.length-3].className === "turnRed"))){
                        return columns1[columns1.length-4].className = "turnRed";
                    }

                    if((columns1[columns1.length-5].className === "circle")&&((columns1[columns1.length-4].className === "turnBlue")||(columns1[columns1.length-4].className === "turnRed"))){
                        return columns1[columns1.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns2.length; i++){
        columns2[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns2[columns2.length-1].className !== "turnBlue")&&(columns2[columns2.length-1].className !== "turnRed")){
                        return columns2[columns2.length-1].className = "turnBlue";

                    }
                    if((columns2[columns2.length-2].className === "circle")&&((columns2[columns2.length-1].className === "turnBlue")||(columns2[columns2.length-1].className === "turnRed"))){
                        return columns2[columns2.length-2].className = "turnBlue";
                    }

                    if((columns2[columns2.length-3].className === "circle")&&((columns2[columns2.length-2].className === "turnBlue")||(columns2[columns2.length-2].className === "turnRed"))){
                        return columns2[columns2.length-3].className = "turnBlue";
                    }

                    if((columns2[columns2.length-4].className === "circle")&&((columns2[columns2.length-3].className === "turnBlue")||(columns2[columns2.length-3].className === "turnRed"))){
                        return columns2[columns2.length-4].className = "turnBlue";
                    }

                    if((columns2[columns2.length-5].className === "circle")&&((columns2[columns2.length-4].className === "turnBlue")||(columns2[columns2.length-4].className === "turnRed"))){
                        return columns2[columns2.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns2[columns2.length-1].className !== "turnBlue")&&(columns2[columns2.length-1].className !== "turnRed")){
                        return columns2[columns2.length-1].className = "turnRed";

                    }
                    if((columns2[columns2.length-2].className === "circle")&&((columns2[columns2.length-1].className === "turnBlue")||(columns2[columns2.length-1].className === "turnRed"))){
                        return columns2[columns2.length-2].className = "turnRed";
                    }

                    if((columns2[columns2.length-3].className === "circle")&&((columns2[columns2.length-2].className === "turnBlue")||(columns2[columns2.length-2].className === "turnRed"))){
                        return columns2[columns2.length-3].className = "turnRed";
                    }

                    if((columns2[columns2.length-4].className === "circle")&&((columns2[columns2.length-3].className === "turnBlue")||(columns2[columns2.length-3].className === "turnRed"))){
                        return columns2[columns2.length-4].className = "turnRed";
                    }

                    if((columns2[columns2.length-5].className === "circle")&&((columns2[columns2.length-4].className === "turnBlue")||(columns2[columns2.length-4].className === "turnRed"))){
                        return columns2[columns2.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns3.length; i++){
        columns3[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns3[columns3.length-1].className !== "turnBlue")&&(columns3[columns3.length-1].className !== "turnRed")){
                        return columns3[columns3.length-1].className = "turnBlue";

                    }
                    if((columns3[columns3.length-2].className === "circle")&&((columns3[columns3.length-1].className === "turnBlue")||(columns3[columns3.length-1].className === "turnRed"))){
                        return columns3[columns3.length-2].className = "turnBlue";
                    }

                    if((columns3[columns3.length-3].className === "circle")&&((columns3[columns3.length-2].className === "turnBlue")||(columns3[columns3.length-2].className === "turnRed"))){
                        return columns3[columns3.length-3].className = "turnBlue";
                    }

                    if((columns3[columns3.length-4].className === "circle")&&((columns3[columns3.length-3].className === "turnBlue")||(columns3[columns3.length-3].className === "turnRed"))){
                        return columns3[columns3.length-4].className = "turnBlue";
                    }

                    if((columns3[columns3.length-5].className === "circle")&&((columns3[columns3.length-4].className === "turnBlue")||(columns3[columns3.length-4].className === "turnRed"))){
                        return columns3[columns3.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns3[columns3.length-1].className !== "turnBlue")&&(columns3[columns3.length-1].className !== "turnRed")){
                        return columns3[columns3.length-1].className = "turnRed";

                    }
                    if((columns3[columns3.length-2].className === "circle")&&((columns3[columns3.length-1].className === "turnBlue")||(columns3[columns3.length-1].className === "turnRed"))){
                        return columns3[columns3.length-2].className = "turnRed";
                    }

                    if((columns3[columns3.length-3].className === "circle")&&((columns3[columns3.length-2].className === "turnBlue")||(columns3[columns3.length-2].className === "turnRed"))){
                        return columns3[columns3.length-3].className = "turnRed";
                    }

                    if((columns3[columns3.length-4].className === "circle")&&((columns3[columns3.length-3].className === "turnBlue")||(columns3[columns3.length-3].className === "turnRed"))){
                        return columns3[columns3.length-4].className = "turnRed";
                    }

                    if((columns3[columns3.length-5].className === "circle")&&((columns3[columns3.length-4].className === "turnBlue")||(columns3[columns3.length-4].className === "turnRed"))){
                        return columns3[columns3.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns4.length; i++){
        columns4[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns4[columns4.length-1].className !== "turnBlue")&&(columns4[columns4.length-1].className !== "turnRed")){
                        return columns4[columns4.length-1].className = "turnBlue";

                    }
                    if((columns4[columns4.length-2].className === "circle")&&((columns4[columns4.length-1].className === "turnBlue")||(columns4[columns4.length-1].className === "turnRed"))){
                        return columns4[columns4.length-2].className = "turnBlue";
                    }

                    if((columns4[columns4.length-3].className === "circle")&&((columns4[columns4.length-2].className === "turnBlue")||(columns4[columns4.length-2].className === "turnRed"))){
                        return columns4[columns4.length-3].className = "turnBlue";
                    }

                    if((columns4[columns4.length-4].className === "circle")&&((columns4[columns4.length-3].className === "turnBlue")||(columns4[columns4.length-3].className === "turnRed"))){
                        return columns4[columns4.length-4].className = "turnBlue";
                    }

                    if((columns4[columns4.length-5].className === "circle")&&((columns4[columns4.length-4].className === "turnBlue")||(columns4[columns4.length-4].className === "turnRed"))){
                        return columns4[columns4.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns4[columns4.length-1].className !== "turnBlue")&&(columns4[columns4.length-1].className !== "turnRed")){
                        return columns4[columns4.length-1].className = "turnRed";

                    }
                    if((columns4[columns4.length-2].className === "circle")&&((columns4[columns4.length-1].className === "turnBlue")||(columns4[columns4.length-1].className === "turnRed"))){
                        return columns4[columns4.length-2].className = "turnRed";
                    }

                    if((columns4[columns4.length-3].className === "circle")&&((columns4[columns4.length-2].className === "turnBlue")||(columns4[columns4.length-2].className === "turnRed"))){
                        return columns4[columns4.length-3].className = "turnRed";
                    }

                    if((columns4[columns4.length-4].className === "circle")&&((columns4[columns4.length-3].className === "turnBlue")||(columns4[columns4.length-3].className === "turnRed"))){
                        return columns4[columns4.length-4].className = "turnRed";
                    }

                    if((columns4[columns4.length-5].className === "circle")&&((columns4[columns4.length-4].className === "turnBlue")||(columns4[columns4.length-4].className === "turnRed"))){
                        return columns4[columns4.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns5.length; i++){
        columns5[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns5[columns5.length-1].className !== "turnBlue")&&(columns5[columns5.length-1].className !== "turnRed")){
                        return columns5[columns5.length-1].className = "turnBlue";

                    }
                    if((columns5[columns5.length-2].className === "circle")&&((columns5[columns5.length-1].className === "turnBlue")||(columns5[columns5.length-1].className === "turnRed"))){
                        return columns5[columns5.length-2].className = "turnBlue";
                    }

                    if((columns5[columns5.length-3].className === "circle")&&((columns5[columns5.length-2].className === "turnBlue")||(columns5[columns5.length-2].className === "turnRed"))){
                        return columns5[columns5.length-3].className = "turnBlue";
                    }

                    if((columns5[columns5.length-4].className === "circle")&&((columns5[columns5.length-3].className === "turnBlue")||(columns5[columns5.length-3].className === "turnRed"))){
                        return columns5[columns5.length-4].className = "turnBlue";
                    }

                    if((columns5[columns5.length-5].className === "circle")&&((columns5[columns5.length-4].className === "turnBlue")||(columns5[columns5.length-4].className === "turnRed"))){
                        return columns5[columns5.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns5[columns5.length-1].className !== "turnBlue")&&(columns5[columns5.length-1].className !== "turnRed")){
                        return columns5[columns5.length-1].className = "turnRed";

                    }
                    if((columns5[columns5.length-2].className === "circle")&&((columns5[columns5.length-1].className === "turnBlue")||(columns5[columns5.length-1].className === "turnRed"))){
                        return columns5[columns5.length-2].className = "turnRed";
                    }

                    if((columns5[columns5.length-3].className === "circle")&&((columns5[columns5.length-2].className === "turnBlue")||(columns5[columns5.length-2].className === "turnRed"))){
                        return columns5[columns5.length-3].className = "turnRed";
                    }

                    if((columns5[columns5.length-4].className === "circle")&&((columns5[columns5.length-3].className === "turnBlue")||(columns5[columns5.length-3].className === "turnRed"))){
                        return columns5[columns5.length-4].className = "turnRed";
                    }

                    if((columns5[columns5.length-5].className === "circle")&&((columns5[columns5.length-4].className === "turnBlue")||(columns5[columns5.length-4].className === "turnRed"))){
                        return columns5[columns5.length-5].className = "turnRed";
                    }
                }
            }
        }
    }

    for(var i = 0; i < columns6.length; i++){
        columns6[i].onclick = function () {
            if(this.className == "circle"){
                if(blue_red%2 === 0){
                    blue_red += 1;
                    if((columns6[columns6.length-1].className !== "turnBlue")&&(columns6[columns6.length-1].className !== "turnRed")){
                        return columns6[columns6.length-1].className = "turnBlue";

                    }
                    if((columns6[columns6.length-2].className === "circle")&&((columns6[columns6.length-1].className === "turnBlue")||(columns6[columns6.length-1].className === "turnRed"))){
                        return columns6[columns6.length-2].className = "turnBlue";
                    }

                    if((columns6[columns6.length-3].className === "circle")&&((columns6[columns6.length-2].className === "turnBlue")||(columns6[columns6.length-2].className === "turnRed"))){
                        return columns6[columns6.length-3].className = "turnBlue";
                    }

                    if((columns6[columns6.length-4].className === "circle")&&((columns6[columns6.length-3].className === "turnBlue")||(columns6[columns6.length-3].className === "turnRed"))){
                        return columns6[columns6.length-4].className = "turnBlue";
                    }

                    if((columns6[columns6.length-5].className === "circle")&&((columns6[columns6.length-4].className === "turnBlue")||(columns6[columns6.length-4].className === "turnRed"))){
                        return columns6[columns6.length-5].className = "turnBlue";
                    }



                } else{

                    blue_red +=1;

                    if((columns6[columns6.length-1].className !== "turnBlue")&&(columns6[columns6.length-1].className !== "turnRed")){
                        return columns6[columns6.length-1].className = "turnRed";

                    }
                    if((columns6[columns6.length-2].className === "circle")&&((columns6[columns6.length-1].className === "turnBlue")||(columns6[columns6.length-1].className === "turnRed"))){
                        return columns6[columns6.length-2].className = "turnRed";
                    }

                    if((columns6[columns6.length-3].className === "circle")&&((columns6[columns6.length-2].className === "turnBlue")||(columns6[columns6.length-2].className === "turnRed"))){
                        return columns6[columns6.length-3].className = "turnRed";
                    }

                    if((columns6[columns6.length-4].className === "circle")&&((columns6[columns6.length-3].className === "turnBlue")||(columns6[columns6.length-3].className === "turnRed"))){
                        return columns6[columns6.length-4].className = "turnRed";
                    }

                    if((columns6[columns6.length-5].className === "circle")&&((columns6[columns6.length-4].className === "turnBlue")||(columns6[columns6.length-4].className === "turnRed"))){
                        return columns6[columns6.length-5].className = "turnRed";
                    }
                }
            }
        }
    }
