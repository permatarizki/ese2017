// While Loops
//
// while loops are a while to have your program continuously run some block of code
// until a condition is met (made TRUE). The syntax is:
//
// while (condition){
//     # Code executed here
//     # while condition is true
// }
// //
// // A major concern when working with a while loop is to make sure that at some
// // point the condition should become true, otherwise the while loop will go forever!
//
// // Example
// var x = 0
//
// while(x < 5){
//
//     console.log("x is currently: "+ x);
//     console.log("x is still less than 5, adding 1 to x");
//
//     // add one to x
//     x = x+1;
//
// }
//
//
// // Can also manually break on conditions, which will exit the loop
// // Example
// var x = 0
//
// while(x < 5){
//
//     console.log("x is currently: "+ x);
//
//     if(x === 3){
//       console.log("x is equal to 3, BREAK")
//       break;
//     }
//
//     console.log("x is still less than 5, adding 1 to x");
//
//     // add one to x
//     x = x+1;
//
// }
//
// // QUICK EXERCISE
//
// // Write a while loop that prints out only the even numbers from 1 to 10.
// var x = 1
// while (x<=10){
//     if(x%2===0){
//         console.log("print x: "+x)
//     }
//     x+=1
// }
//
// // 1-1000 kelipatan 100
//
// var x = 1
// while (x<=1000){
//     if(x%100===0){
//         console.log("Kelipatan 100 = "+x)
//
//     }
//     x+=1
// }

// 20 sampai 2 kelipatan 2

// var x = 20
// while (x>=2){
//     if(x%2===0){
//         console.log("Kelipatan 2 = "+x)
//     }
//     x-=1
// }

// 0,1,3,6,10,15,21,28 ... <100

var x = 0
var y = 0
while (y<=100){
    x+=1
    console.log(y)
    y=y+x
    
}