var lbs = prompt("what is the weight in pounds(lbs)?")
//Welcome to part4 of Javascript Level One!
//It's time to learn how to add some logical capabilities to our JS code.
//You can type these commands into browser console

// Lets review booleans
true
false

//Comparison Operation//

//Greater han
3>2;
4>3;

//less then
1<2;

// Greater then or equal to
2>=2;
//less then or equal to
2<=2;

// Equality
2==2;
"username" == "username";

//Inequality
2!=3;

// Hold on a second ! What happens with this...
"2"==2;

// Check equality of value and type
5 === 5;
5 === "5";

// Check for inequality of value and type
5 !== "5";
5 !== 5;

// its common int programming language for 0 and 1 to subtitutes for
// true or false
true == 1;
true ===1;

false == 0;
false === 0 ;

//weird behaviour for null, undefined, NaN values!

null == undefined;// true

NaN==NaN;// false

// Logical Operator//

// AND is written with &&
// Need both conditions to be true to return true
1===1 && 2===2;

// OR is written with ||
// need only one condition to be true to return true
1===2|| 1===1;

//NOT is written with !
// Basically a way of checking not
!(1==1);
!!(1===1);

//OPTIONAL EXERCISES//

var x =1
var y =2

//Exercise 1
"2" ==y && x===1;

//Exercise 2
x>=0 || y ==="2";

// Exercise 3
!(x!==1) && y ===(1+1);

// Exercise 4
y !== "2" && x <10;

// Exercise 5
y !== x || y == "2" || x === 3;

var lbs = prompt("what is the weight in pounds(lbs)?")
var kg = lbs*0.454
alert ("that is : "+kg+" kilograms")
console.log("Conversion Completed")