// PART 4 ARRAY EXERCISE
// This is  a .js file with commented hints, its optional to use this.

// Create Empty Student Roster Array
// This has been done for you!
var roster = []

// Create the functions for the tasks

// ADD A NEW STUDENT

// Create a function called addNew that takes in a name
// and uses .push to add a new student to the array
function addNew(){
    var newName = prompt("What's the name of new student?")
    if (roster.indexOf(newName) == -1){
        roster.push(newName)
        return
    }
    alert("You already have this name on your data!")
}

// REMOVE STUDENT

// Create a function called remove that takes in a name
// Finds its index location, then removes that name from the roster
function remove(){
    var name = prompt("What's the name you like to remove?")
    var index = roster.indexOf(name)
    if (index == -1){
        alert("No students found!")
    }
    roster.splice(index,1)
}

// HINT: http://stackoverflow.com/questions/5767325/how-to-remove-a-particular-element-from-an-array-in-javascript
//

// DISPLAY ROSTER

// Create a function called display that prints out the roster to the console.
function displayRoster() {
    console.log(roster)
}

// Start by asking if they want to use the web app
var input = prompt("Do you want to use web app?")

// Now create a while loop that keeps asking for an action (add,remove, display or quit)
// Use if and else if statements to execute the correct function for each command.

while (input[0]=="y" || input[0]=="Y"){
    var menu = prompt("What to do? add/remove/display/quit")
    if (menu=="add"){
        addNew()
    } else if (menu=="remove"){
        remove()
    } else if (menu=="display"){
        displayRoster()
    } else if (menu=="quit"){
        break
    } else {
        alert("Please enter a valid input1")
    }
}

alert("Thanks for playing!")
