// Function in JS

function hello(){
    console.log("Hello, world!")
}

function greetName(name){
    console.log("Hello, "+name+"!")
}

function addNumber(num1, num2){
    console.log(num1+num2)
}

// Exercise
function square(number){
    console.log(number**2)
}

function helloSomeone(name="Frankie") {
    console.log("Hello, "+name+"!")
}

// Returning function
function formal(name="Sam", title="Sir") {
    return title+" "+name
}

// Scope = Local variable
function multiply5(number){
    return number*5
}

var v = "I am a global v"
var stuff = "I'm a global stuff"

function fun() {
    console.log(v)
    stuff = "Reassigning stuff"
    console.log(stuff)
}