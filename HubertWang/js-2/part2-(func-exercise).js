// Case: We sleep in if it's not weekday or on a vacation
function sleepIn(weekday, vacation){
    return !weekday || vacation
}

// Case: Troublesome if both monkey or neither of them smiling
function monkeyTrouble(aSmile, bSmile){
    return aSmile == bSmile
}

// Case: Print sub-string str, n times
function stringTimes(str, n) {
    var answer = ""
    for (i=0;i<n;i++){
        answer+=str
    }
    return answer
}

// Case: Sum of all numbers, stop when it's 13 (exclusive)
function luckySum(one,two,three){
    if (one!=13){
        if (two!=13){
            if (three!=13){
                return one+two+three
            }
            return one+two
        }
        return one
    } else {
        return 0
    }
}

// Case: Ticket raise according to speed limit
// Limit is 5 point higher when a person is having birthday
function caughtSpeeding(speed, isBirthday){
    if (isBirthday){
        speed-=5
    }

    if (speed <= 60){
        return 0
    } else if (speed <= 80){
        return 1
    } else {
        return 2
    }
}

// Case: Make bricks
function makeBricks(small, big, goal){
    // if (big==0){
    //     return small>=goal
    // }
    // return
    return goal%5 >= 0 && goal%5 <= small && small+5*big >= goal
}