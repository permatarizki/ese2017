// Print multiple of 100 from 1 to 1000
var num = 100
while (num<=1000){
    console.log(num)
    num+=100
}

// Print even number from 1 to 20 descendingly
num = 20
while (num>0){
    if (num%2===0){
        console.log(num)
    }
    num-=1
}

// Print odd number from 1 to 100
num = 1
numAnswer = ""
while (num<=100){
    if (num%2==1){
        numAnswer+=(num+" ")
    }
    num+=1
}
console.log(numAnswer)

// Print 0,1,3,6,... until 100
num = 0
var incrementer = 0
while (num<=100){
    console.log(num)
    incrementer+=1
    num+=incrementer
}
// for-loop version
num = 0
for (i=0;i<=100;i++){
    if (num>100){
        break
    }
    num+=i
    console.log(num)
}