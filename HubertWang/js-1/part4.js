// MARK: Javascript can be written with semicolon(;) or not

// Booleans
true
false

// Greater than test
3>2;
2>3;

// Less than test
1<2;

// Equality
2==2;
"Ayam"=="Sapi";

// Less than or equal to
3<=5;
5<=10.5;

// More than or equal to
7>=2;
2>=5;

// Inequality
2!=3;
10!=10;

// Hold on a second... What happen with this?
"2"==2;

// Check equality of value and type
5===5;
5==="5";

// Check inequality of value and type
5!==5;
5!=="5";

// It's common in programming languages that 0 and 1 represents
// True and false respectively
1==true;
true===1;

false==0;
0===false;

// Weird behaviour for null, undefined, and NaN values!
NaN == NaN;
null == undefined;

// Conditional
(1===1) && (2=="2");
(1=="1") || (3===4);
!(1===1)
!!(1===1)

// EXERCISE
var x=1
var y=2

// Q.1
"2" == y && x===1

// Q.2
x>=0 || y==="2"

// Q.3
!(x !== 1) && y === (1+1)

// Q.4
y !== "2" && x<10

// Q.5
y !==x || y=="2" || x==3